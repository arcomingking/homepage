import React from 'react';
import { Row, Col, Container, InputGroup, FormControl, Button } from 'react-bootstrap';
import styled from 'styled-components';
import FA from 'react-fontawesome';

const Styles = styled.div`
    .row-footer {
        background-color: #7e9292;
        padding: 40px 0px 20px 0px;
    }

    .footer-col-title {
        color: #ffffff;
        font-size: 20px;
        font-weight: bold;
    }

    .footer-ul li{
        list-style-type: none;
        line-height: 30px;
        margin-left: -20px;
    }

    .footer-ul li a{
        text-decoration: none;
        color: #dedede;
    }
    
    .social-media li {
        display: inline;
        list-style-type: none;
        line-height: 45px;
        letter-spacing: 55px;
    }

    .sm-icon {
        font-size: 55px;
        color: #044954;
    }

    .copy-right {
        height: 50px;
        width: 50%;
        padding: 14px;
        float: left;
    }

    .copy-right-row {
        background-color: #426a6a;
        color: #ffffff;
    }
`;

export const Footer = () => (
    <Styles>
        <Container fluid>
            <Row className="row-footer">
                <Container>
                    <Row>
                        <Col md="3">
                        <span className="footer-col-title">Company</span>
                            <ul className="footer-ul">
                                <li><a href="#">Our Story</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </Col>
                        <Col md="3">
                            <span className="footer-col-title">Product</span>
                            <ul className="footer-ul">
                                <li><a href="#">Email</a></li>
                                <li><a href="#">Mobile</a></li>
                                <li><a href="#">Web</a></li>
                            </ul>
                        </Col>
                        <Col md="6">
                            <span className="footer-col-title">Stay Connected</span>
                            <InputGroup className="mb-3">
                                <FormControl
                                placeholder="Email Address"
                                aria-label="Email Address"
                                aria-describedby="basic-addon2"
                                />
                                <InputGroup.Append>
                                <Button variant="secondary">Sign-Up</Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <ul className="social-media">
                                <li><FA className="sm-icon" name="facebook-square"/></li>
                                <li><FA className="sm-icon" name="instagram"/></li>
                                <li><FA className="sm-icon" name="twitter-square"/></li>
                                <li><FA className="sm-icon" name="youtube-play"/></li>
                            </ul>
                        </Col>
                    </Row>
                </Container>
            </Row>
            <Row className="copy-right-row">
                <span className="copy-right">© 2019 All Rights Reserved.</span>
            </Row>
        </Container>
    </Styles>
);