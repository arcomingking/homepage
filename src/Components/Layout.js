import React from 'react';
import { Container } from 'react-bootstrap';
import { expressionStatement } from '@babel/types';

export const Layout = (props) => (
    <Container fluid>
        { props.children }
    </Container>
);