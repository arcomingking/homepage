import React from 'react';
import { Jumbotron as Jumbo, Container, Button } from 'react-bootstrap';
import styled from 'styled-components';

const Styles = styled.div`
    .jumbo {
        text-align: center;
        background-color: #07a8ad;
        height: 400px;
    }

    .jumbo .container h1 {
        color: #ffffff;
    }

    .jumbo .container p {
        color: #ffffff;
        text-align: justify;
    }
`;


export const Jumbotron = () => (
    <Styles>
        <Jumbo fluid className="jumbo">
            <Container>
                <h1>Volutpat consequat</h1>
                <p>
                Volutpat consequat mauris nunc congue nisi vitae. Auctor augue mauris augue neque gravida in fermentum. Odio eu feugiat pretium nibh. Dolor sit amet consectetur adipiscing elit ut. Adipiscing elit pellentesque habitant morbi. Suspendisse in est ante in nibh mauris cursus mattis.
                </p>
                <Button variant="secondary" size="lg">
                    Get Started
                </Button>
            </Container>
        </Jumbo>
    </Styles>
);