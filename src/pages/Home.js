import React from "react";
import { Row, Col, Container } from 'react-bootstrap';
import styled from 'styled-components';
import FA from 'react-fontawesome';

const Styles = styled.div`
    .row-top {
        padding: 40px 0 40px 0;
    }

    .row-bottom {
        padding: 40px 0 40px 0;
        background-color: #e2f3f5;
    }

    .fa-address-card {
        font-size: 250px;
        margin-left: 10px;
        color: #26595f;
        width: 100%;
        text-align: center;
    }

    .fa-dollar {
        font-size: 250px;
        color: #26595f;
        width: 100%;
        text-align: center;
    }

    .text-head {
        padding-top: 20px;
        font-weight: bold;
        text-align: justify;
    }

    .text-body {
        font-size: 13px;
        text-align: justify;
    }
`;

export const Home = () => (
    <Styles>
        <Row className="row-top">
            <Container>
                <Row>
                    <Col md="9">
                        <h2>Lorem ipsum</h2>
                        <p className="text-head">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <p className="text-body">Sagittis id consectetur purus ut faucibus pulvinar elementum integer. Amet volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Enim neque volutpat ac tincidunt vitae semper quis lectus. Risus nec feugiat in fermentum posuere urna nec. Eu scelerisque felis imperdiet proin fermentum leo. Mattis nunc sed blandit libero volutpat sed cras ornare. In ornare quam viverra orci. Erat velit scelerisque in dictum non consectetur a erat.</p>
                    </Col>
                    <Col md="3">
                        <FA name="address-card" />
                    </Col>
                </Row>
            </Container>
        </Row>
        <Row className="row-bottom">
            <Container>
                <Row>
                    <Col md="3">
                        <FA name="dollar" />
                    </Col>
                    <Col md="9">
                        <h2>Lorem ipsum</h2>
                        <p className="text-head">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <p className="text-body">Sagittis id consectetur purus ut faucibus pulvinar elementum integer. Amet volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Enim neque volutpat ac tincidunt vitae semper quis lectus. Risus nec feugiat in fermentum posuere urna nec. Eu scelerisque felis imperdiet proin fermentum leo. Mattis nunc sed blandit libero volutpat sed cras ornare. In ornare quam viverra orci. Erat velit scelerisque in dictum non consectetur a erat.</p>
                    </Col>
                </Row>
            </Container>
        </Row>
    </Styles>
)