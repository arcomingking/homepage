import React from "react";

export const NoMatch = () => (
    <div>
        <h2>Error</h2>
        <p>Error 404:  Not Found.</p>
    </div>
);